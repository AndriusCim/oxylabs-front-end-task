import { useContext, useEffect, useState } from 'react';
import { useApi, useAbortController } from './useApi';
import { getServers, IServer } from '../api/servers';
import { UserContext } from '../context/UserContextProvider';

interface State {
  data: IServer[] | null;
  errors: any;
  loading: boolean;
}

const initialState: State = {
  data: null,
  errors: null,
  loading: false
};

const loadStart = () => (prevState: State): State => ({
  ...prevState,
  loading: true
});

const loadSuccess = (data: IServer[]) => (prevState: State): State => ({
  ...prevState,
  data,
  loading: false
});

const loadFailure = (errors: any) => (prevState: State): State => ({
  ...prevState,
  errors,
  loading: false
});

export const useServers = () => {
  const api = useApi();
  const { signal } = useAbortController();
  const { state: userState } = useContext(UserContext);
  const [state, setState] = useState(initialState);

  const fetchServers = async () => {
    try {
      setState(loadStart());
      const data = await api.call(getServers());
      if (!signal.aborted) {
        setState(loadSuccess(data));
      }
    } catch (ex) {
      if (!signal.aborted) {
        console.error('Failed to load data', ex);
        setState(loadFailure(ex));
      }
    }
  };

  const sortedServerList = !!state.data
    ? state.data.sort((a, b) => a.distance - b.distance || a.name.localeCompare(b.name))
    : null;

  useEffect(() => {
    fetchServers();
  }, [userState]);

  return { ...state, data: sortedServerList };
};
