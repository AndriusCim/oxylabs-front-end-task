import { useEffect, useRef } from 'react';

// useApiClient
interface ApiOptions {
  json?: any;
  signal?: AbortSignal;
}

type ApiResponse = Promise<Response> & {
  json<T = unknown>(): Promise<T>;
};

const request = async (method: 'GET' | 'POST', url: string, options: ApiOptions) => {
  const apiEndpoint = 'https://playground.tesonet.lt/v1';

  const headers: Record<string, string> = {
    accept: 'application/json',
    'content-type': 'application/json;charset=UTF-8'
  };

  if (localStorage.getItem('token')) {
    headers['authorization'] = `Bearer ${localStorage.getItem('token')}`;
  }

  const httpOptions: RequestInit = {
    method,
    body: JSON.stringify(options.json),
    headers,
    signal: options.signal
  };

  const response = await fetch(`${apiEndpoint}/${url}`, httpOptions);

  if (!response.ok) {
    if (response.status === 400) {
      const result = await response.json();
      if (typeof result === 'object' && (typeof result.code !== 'undefined' || typeof result.Code !== 'undefined')) {
        console.error(result.code || result.Code, result.details || result.Details);
      }
    }
    console.error(response.statusText || response);
  }

  return response;
};

const wrapResponse = (response: Promise<Response>): ApiResponse => {
  return Object.assign(response, {
    json: () => response.then((result) => result.json())
  });
};

export const useApiClient = () => {
  return {
    get: (url: string, options?: ApiOptions) => wrapResponse(request('GET', url, options || {})),
    post: (url: string, options?: ApiOptions) => wrapResponse(request('POST', url, options || {}))
  };
};

export type ApiClient = ReturnType<typeof useApiClient>;
// useAbortController
export const useAbortController = () => {
  const abortController = useRef(new AbortController());

  useEffect(() => () => abortController.current.abort(), []);

  return abortController.current;
};

export type ApiRequest<T> = (api: { apiClient: ApiClient; signal: AbortSignal | undefined }) => Promise<T>;

export interface Api {
  call: <T>(request: ApiRequest<T>, signal?: AbortSignal) => Promise<T>;
}
// useApi
export const useApi = (): Api => {
  const apiClient = useApiClient();

  return {
    call: <T>(request: ApiRequest<T>, signal?: AbortSignal) => request({ apiClient, signal })
  };
};
