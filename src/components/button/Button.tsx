import React, { ComponentPropsWithoutRef, ReactNode } from 'react';
import clsx from 'clsx';
import spinner from '../../styles/assets/icon-spinner.svg';
import styles from './Button.module.scss';

const sizeClassnames = {
  big: styles['btn--big'],
  small: styles['btn--small']
};

const colorClassnames = {
  primary: styles['btn--primary'],
  secondary: styles['btn--secondary']
};

interface Props extends ComponentPropsWithoutRef<'button'> {
  size?: keyof typeof sizeClassnames;
  color?: keyof typeof colorClassnames;
  loading?: boolean;
  icon?: ReactNode;
}

const Button: React.FC<Props> = ({
  children,
  size = 'big',
  color = 'primary',
  disabled,
  loading,
  icon,
  className = '',
  ...props
}) => {
  return (
    <button
      disabled={disabled || loading}
      className={clsx(sizeClassnames[size], colorClassnames[color], styles['btn'], className)}
      data-testid="button"
      {...props}
    >
      {icon ? <span>{icon}</span> : null}
      <span className={styles['btn__text']}>{children}</span>
      {loading ? <img src={spinner} className={styles['btn__spinner']} /> : null}
    </button>
  );
};

export default Button;
