import React from 'react';
import { mount } from 'enzyme';
import Button from './Button';
import icon from '../../styles/assets/icon-lock.svg';

describe('<Button />', () => {
  it('should render button with correct props', () => {
    const wrapper = mount(
      <Button color="primary" size="big" icon={<img src={icon} />} loading>
        Test button
      </Button>
    );
    expect(wrapper.props().color).toEqual('primary');
    expect(wrapper.props().loading).toEqual(true);
    expect(wrapper.props().size).toEqual('big');
    expect(wrapper.props().icon).toEqual(<img src={icon} />);
  });

  it('should render loading button', () => {
    const wrapper = mount(
      <Button color="primary" loading>
        Loading button
      </Button>
    );
    expect(wrapper.props().color).toEqual('primary');
    expect(wrapper.props().loading).toEqual(true);
  });

  it('should render icon in button', () => {
    const wrapper = mount(
      <Button icon={<img src={icon} />} color="primary">
        Button with icon
      </Button>
    );
    expect(wrapper.props().icon).toEqual(<img src={icon} />);
    expect(wrapper.find('img')).toHaveLength(1);
  });
});
