import React, { ComponentPropsWithoutRef } from 'react';
import clsx from 'clsx';
import styles from './Input.module.scss';

export interface Props extends ComponentPropsWithoutRef<'input'> {
  loading?: boolean;
}

const Input: React.FC<Props> = ({ loading, className, disabled, ...props }) => {
  return (
    <input disabled={disabled || loading} className={clsx(styles['input'], className)} data-testid="input" {...props} />
  );
};

export default Input;
