import React, { useContext } from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';

import { UserContext } from '../context/UserContextProvider';
import { path } from '../paths';

const PrivateRoute: React.FC<RouteProps> = (props) => {
  const { state } = useContext(UserContext);
  const { component: Component, ...rest } = props;

  return (
    <Route
      {...rest}
      render={(routeProps) =>
        !!state.token ? (
          <Component {...routeProps} />
        ) : (
          <Redirect
            to={{
              pathname: path.login,
              state: { from: routeProps.location }
            }}
          />
        )
      }
    />
  );
};

export default PrivateRoute;
